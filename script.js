//1. Create a readingListActD folder. Inside create an index.html and index.js files for the first part of the activity and create a crud.js file for the second part of the activity.
//2. Once done with your solution, create a repo named 'readingListActD' and push your documents.
//3. Save the repo link on S32-C1

//Part 1:
/*
Create a readingListActD folder. Inside, create an index.html and index.js file. Test the connection of your js file to the html file by printing 'Hello World' in the console.
1.)
Create a student class sectioning system based on their entrance exam score.
If the student average is from 80 and below. Message: Your section is Grade 10 Section Ruby,
If the student average is from 81-120. Message: Your section is Grade 10 Section Opal,
If the student average is from 121-160. Message: Your section is Grade 10 Section Sapphire,
If the student average is from 161-200 to. Message: Your section is Grade 10 Section Diamond

Sample output in the console: Your score is (score). You will become proceed to Grade 10 (section)

2.) 
Write a JavaScript function that accepts a string as a parameter and find the longest word within the string.

Sample Data and output:
Example string: 'Web Development Tutorial'
Expected Output: 'Development'

3.)
Write a JavaScript function to find the first not repeated character.

Sample arguments : 'abacddbec'
Expected output : 'e'

*/




// 1

const sectioningSystem = function (math, science, history, english) {

  const average = (math + science + history + english) / 4;

  let section;

  if (average <= 80) {
    section = 'Ruby';
  } else if (average >= 81 && average <= 120) {
    section = 'Opal';
  } else if (average >= 121 && average <= 160) {
    section = 'Sapphire';
  } else if (average >= 161 && average <= 200) {
    section = 'Diamond';
  }

    return console.log(`Your score is ${average}. You will be joining Grade 10 Section ${section}.`);
}

sectioningSystem(90,95,93,89);


// 2

const findTheLongestWord = function (yourString) {

      const yourWords = yourString.split(" ");
      let wordIndex = 0;
      let longestWord = yourWords[0];

        for (let i=0; i<yourWords.length; i++) {
            if (i < yourWords.length-1) {
                    wordIndex++;
            }

            const nextWord = yourWords[wordIndex];
            if (longestWord.length < nextWord.length) {
              longestWord = nextWord;
            } 
        }

  return console.log(longestWord);
}

findTheLongestWord('Web Development Tutorial');


// 3

let yourString;
let notRepeated = "";

const checkNotRepeated = function (str) {
      yourString = Array.from(str);
      for (let i=0; i<str.length; i++) {
          const compareLetter = yourString.splice(i, 1);

         if (!yourString.includes(compareLetter[0])) {
          notRepeated = notRepeated + compareLetter[0];
         }
         yourString = Array.from(str);
      }
        return console.log(notRepeated[0]);
  }

checkNotRepeated('abaxcddbecq');















