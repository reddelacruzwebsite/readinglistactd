//Part 2:
/*
Create a simple server and the following routes with their corresponding HTTP methods and responses:
  If the url is http://localhost:8000/, send a response Welcome to Ordering System
  If the url is http://localhost:8000/dashboard, send a response Welcome to your User's Dashboard!
  If the url is http://localhost:8000/products, send a response Here’s our products available
  If the url is http://localhost:8000/addProduct, send a response Add a course to our resources
      - create a mock datebase of products that has these fields: (name, description, price, stocks)
      - use the request_body to add new products.
  If the url is http://localhost:8000/updateProduct, send a response Update a course to our resources
  If the url is http://localhost:8000/archiveProduct, send a response Archive courses to our resources
Test each endpoints in POSTMAN and save the screenshots

*/


const http = require('http');

let products = [
  {
    "name": "Adidas Nite Jogger 1",
    "description": "running shoes",
    "price": 100,
    "stocks": 10
  },
  {
    "name": "Adidas Nite Jogger 2",
    "description": "running shoes",
    "price": 120,
    "stocks": 12
  }
];

const port = 8000;
const server = http.createServer(function(request, response) {
  if (request.url == '/dashboard') {
   
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end(`Welcome to your User's Dashboard!`)

  } else if (request.url == '/products' && request.method == 'GET') {

    response.writeHead(200,{'Content-Type': 'text/plain'});
      response.write(`Here’s our products available.`)
    response.end(JSON.stringify(products))
  
  } else if (request.url == '/addProduct' && request.method == 'GET') {
    response.writeHead(200,{'Content-Type': 'text/plain'});
    response.end(`Add a product to our resources`)
} else if (request.url == '/addProduct' && request.method == 'POST') {

    let request_body = ' '

    request.on('data', function(data){
      request_body += data
      console.log(request_body)
    })

    request.on('end', function() {
      console.log(typeof request_body)

      request_body = JSON.parse(request_body)

      let new_product = {
        "name": request_body.name,
        "description": request_body.description,
        "price": request_body.price,
        "stocks": request_body.stocks

      }
      console.log(new_product)
      products.push(new_product)
      console.log(products)

      response.writeHead(200, {'Content-Type': 'application/json'})
      response.write(JSON.stringify(new_product))
      response.end()
    })

  } else if (request.url == '/updateProduct') {
    response.writeHead(200,{'Content-Type': 'text/plain'});
    response.end(`Update a product to our resources`)
  } else if (request.url == '/archiveProduct') {
    response.writeHead(200,{'Content-Type': 'text/plain'});
    response.end(`Archive products to our resources`)
  } else {
    response.writeHead(200, {'Content-Type': 'text/plain'})
    response.end('Welcome to Ordering System')
  }
  })

server.listen(port);

console.log(`Server now accessible at localhost: ${port}`)